/*
 * Complete the 'solve' function below.
 *
 * The function is expected to return a STRING_ARRAY.
 * The function accepts STRING_ARRAY names as parameters.
 */

const NAMES = [
  '100',
  'ghfd',
  'gcjbch',
  'j',
  'gagc',
  'bihjcac',
  'hfjc',
  'ijh',
  'gbcj',
  'bjeh',
  'efadgbagd',
  'agb',
  'fehgfg',
  'dhefcfehee',
  'ahig',
  'iedbejcag',
  'jcggejfae',
  'hbhchccgb',
  'g',
  'fj',
  'jajbh',
  'bbfjhhgh',
  'gfgd',
  'eibcjdjaii',
  'ajgdif',
  'bbfjiei',
  'ad',
  'e',
  'eehgd',
  'hf',
  'gcbhifhebi',
  'jhfdii',
  'bijg',
  'dddig',
  'e',
  'iijhhgeda',
  'ajcf',
  'afjeg',
  'ccehhfeibc',
  'jdgiacbaf',
  'ba',
  'fagegcfig',
  'ieh',
  'eag',
  'jja',
  'bdbbadead',
  'bjgjddiafg',
  'eaaegcg',
  'fgjihcic',
  'jgachgbdcb',
  'jjbejb',
  'h',
  'ihaeia',
  'cjgba',
  'cccaf',
  'cjacid',
  'aeajbjgcf',
  'ejjdg',
  'f',
  'c',
  'edfbhedbeg',
  'eccgebciij',
  'iii',
  'ididddi',
  'e',
  'gijagihj',
  'd',
  'dhdc',
  'fcgfihj',
  'aebaeih',
  'i',
  'cehjdjc',
  'dabhijbfe',
  'cfhejjefjd',
  'haibjj',
  'icfdejac',
  'b',
  'gcbcahdbbj',
  'f',
  'hheageh',
  'ificg',
  'g',
  'egcijga',
  'ababdhhc',
  'efcacja',
  'jefbadhiig',
  'e',
  'hgajhfj',
  'ifddidhj',
  'hihe',
  'ja',
  'iifiedhbdi',
  'j',
  'jjdcedhb',
  'cac',
  'hf',
  'he',
  'hb',
  'bh',
  'e',
  'i',
];

function findDiff(prevName: string, name: string) {
  const length = name.length > prevName.length ? prevName.length : name.length;

  for (let k = 0; k < length; k++) {
    const character = name[k];
    if (character !== prevName[k]) {
      return name.substring(0, k + 1);
    }
  }

  return name.length > prevName.length ? name.substring(0, prevName.length + 1) : name;
}

function solve(names: string[]) {
  // Write your code here
  const groups: Map<string, number> = new Map<string, number>();

  const groupNames = [];

  for (let i = 0; i < names.length; i++) {
    // Trim name
    const name = names[i].trim();
    // Just check with not empty names
    if (!name) {
      continue;
    }

    if (i === 0) {
      groups.set(name, 1);
      groupNames.push(name[0]);
    } else {
      // Find group that match with the whole name
      const countGroup = groups.get(name);

      // If group exists, then increase count
      if (countGroup) {
        groups.set(name, countGroup + 1);
        groupNames.push(`${name} ${countGroup + 1}`);
      } else {
        // Find longest diff with previous names
        let longestDiff = '';
        for (let j = 0; j < i; j++) {
          const prevName = names[j].trim();
          const diff = findDiff(prevName, name);
          longestDiff = diff.length > longestDiff.length ? diff : longestDiff;
        }
        groups.set(name, 1);
        groupNames.push(longestDiff);
      }
    }
  }

  return groupNames;
}

function main() {
  const res = solve(NAMES);
  console.log((res || []).join('\n'));
}

main();

## Getting Started

First, install dependencies:

```bash
npm install
# or
yarn
```

Second, run test:

```bash
npm start
# or
yarn start
```
